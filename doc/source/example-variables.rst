Variables
---------

.. var:: example-variable

   This is an example variable.

   .. var:: foo

      This is a variable.

      .. var:: bar

         This is a sub key.

   .. var:: items
      :type: list

      This variable is a list.

      .. var:: baz

         This is an item in a list.

This is a variable role: :var:`example-variable.items.baz`
