Statistics
----------

.. stat:: example-stat

   This is an example statistic.

   .. stat:: foo
      :type: counter

      A sub stat.

This is a statistics role: :stat:`example-stat.foo`
