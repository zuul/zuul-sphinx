Project Templates
=================

.. project_template:: example

   This is an example project template.  It contains the following jobs:

   **check**

      * :job:`example`
      * :job:`example`

   **gate**

      * :job:`example`

This is a project_template role: :project_template:`example`
